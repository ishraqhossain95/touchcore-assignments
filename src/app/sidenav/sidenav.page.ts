import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'sidenav',
  templateUrl: './sidenav.page.html',
  styleUrls: ['./sidenav.page.scss']
})
export class SidenavPage implements OnInit {
  @Output() onSidenavToggleClick = new EventEmitter();
  @Input() collapsed:boolean = false;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(){
   
  }

  onToggleSidenav(){
    this.collapsed = !this.collapsed;
    // if(this.collapsed){
    //   let container:any = document.querySelector('.sidenav-container');
    //   container.style.cssText = "max-width : 80px!important";

    // }
    this.onSidenavToggleClick.emit(this.collapsed);
  }

}
