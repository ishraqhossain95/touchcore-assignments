import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-work',
  templateUrl: './work.page.html',
  styleUrls: ['./work.page.scss'],
})
export class WorkPage implements OnInit {
 credentials:any = {
    email: "",
    password: ""
  }

  
  activeCategory:string = 'All';
//   taskList = [{
//     item: 'Implement task sorting',
//     dateTimeStamp: '2023-03-15T14:59:11.939Z',
//     selected: false
//   },
//   {
//     item: 'Finishing desing for new website',
//     dateTimeStamp: '2023-03-18T14:59:11.939Z',
//     selected: false
//   },
//   {
//     item: 'Reply to all post today',
//     dateTimeStamp: '2023-03-21T14:59:11.939Z',
//     selected: false
//   },
//   {
//     item: 'Correct mail sending form',
//     dateTimeStamp: '2023-03-24T14:59:11.939Z',
//     selected: false
//   }

// ]
  constructor(
    private authService: AuthService,
    private router: Router

  ) { }

  allData:any[] = [];
  taskData:any[] = [];
  params = {
    itemId: '63e389f16a0dc4c3412a0ff0',
    collectionId: '63e383e2e30d9c576530b859'
  }
  ngOnInit() {
   
  }

  getItems(){
    this.authService.getItem(this.params)
    .subscribe((res:any) => {
      this.allData = res;
      this.taskData = res;
    })
  }

  login(){
    this.authService.login(this.credentials)
    .subscribe(res => {
        localStorage.setItem('token', res);
        this.getItems();
    }),(err:any) => {
      console.log(err);
    }
  }

  async filterData(filterBy:string){
    var data = [];
    if(filterBy != 'All'){
      data =  this.allData.filter(item => {
        return item.priority == filterBy;
      })
    }
    else data = this.allData
    return data;
  }

  async onClickCat(cat:string){
    this.activeCategory = cat;
    this.taskData = await this.filterData(cat);
  }

}
