import { FormsModule } from '@angular/forms';
import { LayoutsPageModule } from './layouts/layouts.module';
import { LayoutsPage } from './layouts/layouts.page';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { InterceptorService } from './interceptor.service';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule,FormsModule ,IonicModule.forRoot(), AppRoutingModule, LayoutsPageModule, HttpClientModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy}, 
    {
      useClass: InterceptorService , provide:HTTP_INTERCEPTORS , multi: true

    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
