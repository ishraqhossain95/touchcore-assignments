import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layouts',
  templateUrl: './layouts.page.html',
  styleUrls: ['./layouts.page.scss'],
})
export class LayoutsPage implements OnInit {
  collapsed:boolean =false;
  displayWidth:number = 0;
  constructor(

  ) { }

  ngOnInit() {
    this.debounce(this.getWidth());
  }
  

  ngAfterViewInit(){
    window.addEventListener("resize",this.debounce((e:any) => {
      this.getWidth();
    }));
  }

  debounce(fn:any){
    var timer:any;
    return (event:any) => {
      if(timer) clearTimeout(timer);
      timer = setTimeout(fn,1000,event);
    };
  }

 

  // onResize(){
  //   this.debounce(this.getWidth());
  // }

  getWidth(){
    this.displayWidth = window.innerWidth;
    console.log(this.displayWidth);

  }



}
