import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutsPage } from './layouts.page';

const routes: Routes = [
  {
    path: '',
    component: LayoutsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutsPageRoutingModule {}
