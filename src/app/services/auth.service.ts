import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly apiBaseUri = environment.apiBaseUri;
  constructor(
    private http: HttpClient
  ) { }

  login(params:any): Observable<any>{
    let headers = {
      responseType: 'text',
      'content-type': 'text/plain; charset=utf-8'
    }
    // headers = headers.set('Response-Type', 'text; charset=utf-8');
      return this.http.get(`${this.apiBaseUri}/Authorize?email=${params.email}&password=${params.password}`, {
        responseType: 'text'
      })
  }


  getItem(params:any){
    return this.http.get(`${this.apiBaseUri}/GetItems/${params.collectionId}/${params.itemId}`);

  }
}
