import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor() { }

  getMenus(){
    return [
      {
        path: '/home/work',
        name: 'Home'
      },
      {
        path: '/home/todos',
        name: 'Todos'
      },
      {
        path: '/home/settings',
        name: 'Settings'
      },
      {
        path: '/home/support',
        name: 'Support'
      },
      
      {
        path: 'logout',
        name: 'Log out'
      }
    ]
  }
}
