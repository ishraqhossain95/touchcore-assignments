import { Component, OnInit } from '@angular/core';
import { MenuService } from '../services/menu.service';

@Component({
  selector: 'topnav',
  templateUrl: './topnav.page.html',
  styleUrls: ['./topnav.page.scss'],
})
export class TopnavPage implements OnInit {
  menus:any[] = [];
  constructor(
    private menuService: MenuService

  ) { }

  ngOnInit() {
    this.menus = this.menuService.getMenus();
  }

}
