export const environment = {
  production: true,
  apiBaseUri: 'https://app.accelerator-platform.com/api/v1'
};
