
# Assignment Done by Ishraq Hossain

I have used Ionic UI, Angular, HTML and SCSS to build this app.


## Installation

1. As it is Ionic Angular Project
So First You have to install Ionic cli globally.

```bash
 npm i @ionic/cli
```
2. Go project folder and do npm install. Because I removed node_modules folder.

```bash
 npm i
```

3. Atlast run ionic serve.

```bash
 ionic serve
```



    
## Feedback

If you have any feedback or issue while running this project, please reach out to us at hi@ishraq.in 

To get more details about me visit my portfolio site https://ishraq.in


